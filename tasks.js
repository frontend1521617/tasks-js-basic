// Solutions for homework tasks

// Task 1 Longest Common Prefix
var longestCommonPrefix = function (strs) {
	let prefix = ''
	let firstStr = strs[0]
	for (let i = 0; i < firstStr.length; i++) {
		if (strs.every(str => firstStr[i] === str[i])) {
			prefix += firstStr[i]
		} else {
			break
		}
	}
	return prefix
}

// Task 2 Count Primes
var countPrimes = function (n) {
	const isPrime = new Array(n).fill(true)
	let count = 0
	for (let i = 2; i < n; i++) {
		if (isPrime[i]) {
			count++
			for (let j = i * 2; j < n; j += i) {
				isPrime[j] = false
			}
		}
	}
	return count
}

// Task 3 Roman to Integer
var romanToInt = function (s) {
	let res = 0
	let i = 0
	if (1 <= s.length && s.length <= 15) {
		const symbols = {
			I: 1,
			IV: 4,
			V: 5,
			IX: 9,
			X: 10,
			XL: 40,
			L: 50,
			XC: 90,
			C: 100,
			CD: 400,
			D: 500,
			CM: 900,
			M: 1000,
		}

		while (i < s.length) {
			if (symbols[s[i] + s[i + 1]]) {
				res += symbols[s[i] + s[i + 1]]
				i += 2
			} else {
				res += symbols[s[i]]
				i++
			}
		}
	}
	return res
}

// Task 4 Merge Sorted Array
var merge = function (nums1, m, nums2, n) {
	let i1 = m - 1
	let i2 = n - 1
	let i = n + m - 1
	while (i >= 0) {
		if ((nums1[i1] < nums2[i2] && i2 > -1) || i1 < 0) {
			nums1[i] = nums2[i2]
			i2--
		} else {
			nums1[i] = nums1[i1]
			i1--
		}
		i--
	}
}

// Task 5 Shuffle an Array
var Solution = function (nums) {
	this.nums = [...nums]
	this.arr = nums
}

Solution.prototype.reset = function () {
	this.arr = [...this.nums]
	return this.arr
}

Solution.prototype.shuffle = function () {
	let arr = this.arr
	for (let i = arr.length - 1; i > 0; i--) {
		let j = Math.floor(Math.random() * (i + 1))
		let t = arr[i]
		arr[i] = arr[j]
		arr[j] = t
	}
	return arr
}
